About:

The goal of this project is to convert all of the known various date
formats into the ISO 8601 standard. If there is ambiguity, then a list
of all possible dates will be returned.

Examples:

    Input         Output

  2015-01-31 -> [2015-01-31]
  01/31/15   -> [2015-01-31]
  1-31-15    -> [2015-01-31]
  

Ambiguous examples:

   Input          Output

  1/2/15    -> [2015-01-02, 2015-02-01]
  

----------------------------

Usage:

To convert a string representing a date from user input, pass the
string to the convert_date(s) function.

This will return one of :
    - An empty list if the input did not represent a valid date.
    - A list of one date (as a string) if there is no ambiguity in the input.
    - A list of multiple dates (as strings) if the input was ambiguous.
