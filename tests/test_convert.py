import datetime
import unittest

import convert
import mock_datetime

class TestConvertDate(unittest.TestCase):
    def test_alpha_count(self):
        self.assertEqual(convert.alpha_count(''), 0)
        self.assertEqual(convert.alpha_count('a'), 1)
        self.assertEqual(convert.alpha_count('2015-09-13'), 0)
        self.assertEqual(convert.alpha_count('Dec. 3, 1999'), 3)
        self.assertEqual(convert.alpha_count('January fifteenth 2015'),
                         len('Januaryfifteenth'))

        self.assertEqual(convert.alpha_count('January 15th 2015'),
                         len('Januaryth'))

    def test_is_leap_year(self):
        # Test true leap year cases (years divisible by 4 and not 100).
        for leapYear in [1992, 1996, 2000, 2004]:
            self.assertTrue(convert.is_leap_year(leapYear))

        # Test non-leap year cases (years not divisible by 4).
        for nonLeapYear in [1997, 1998, 1999, 2001, 2002, 2003, 2005]:
            self.assertFalse(convert.is_leap_year(nonLeapYear))

        # Test non-leap year cases (years divisible by 100 and not 400).
        for nonLeapYear in [2100, 2200, 2300, 2500, 2600, 2700]:
            self.assertFalse(convert.is_leap_year(nonLeapYear))

        # Test true leap year cases (years divisible by 400).
        for leapYear in [400, 800, 1200, 1600, 2000, 2400, 2800]:
            self.assertTrue(convert.is_leap_year(leapYear))

    def test_days_in_month(self):
        # The year parameter doesn't matter for non-February months.
        # These test the 11 non-february months.
        for month in [1, 3, 5, 7, 8, 10, 12]:
            self.assertEqual(convert.days_in_month(month, 0), 31)
        for month in [4, 6, 9, 11]:
            self.assertEqual(convert.days_in_month(month, 0), 30)

        # This tests February for leap years.
        for leapYear in [1600, 1996, 2000, 2004, 2008, 2400, 2800]:
            self.assertEqual(convert.days_in_month(2, leapYear), 29)

        # This tests february for non-leap years.
        for nonLeapYear in [1997, 1998, 1999, 2001, 2002, 2003, 2100, 2200]:
            self.assertEqual(convert.days_in_month(2, nonLeapYear), 28)

        # Test errors
        self.assertEqual(convert.days_in_month(-1, 0), 0)
        self.assertEqual(convert.days_in_month(0, 0), 0)
        self.assertEqual(convert.days_in_month(13, 0), 0)
        self.assertEqual(convert.days_in_month(14, 0), 0)

    def test_format_year(self):
        # Both have two digits.
        self.assertEqual(convert.format_year(10, 10), '1010')
        self.assertEqual(convert.format_year(99, 99), '9999')
        self.assertEqual(convert.format_year(20, 15), '2015')

        # First has one digit, second has two digits.
        self.assertEqual(convert.format_year(0, 99), '0099')
        self.assertEqual(convert.format_year(1, 10), '0110')
        self.assertEqual(convert.format_year(9, 15), '0915')

        # First has two digits, second has one digit.
        self.assertEqual(convert.format_year(99, 0), '9900')
        self.assertEqual(convert.format_year(10, 1), '1001')
        self.assertEqual(convert.format_year(15, 9), '1509')

        # Both have one digit.
        self.assertEqual(convert.format_year(0, 0), '0000')
        self.assertEqual(convert.format_year(9, 9), '0909')
        self.assertEqual(convert.format_year(1, 5), '0105')

        # First has three digits, second has one or two digits.
        self.assertEqual(convert.format_year(111, 99), '11199')
        self.assertEqual(convert.format_year(999, 10), '99910')
        self.assertEqual(convert.format_year(123, 9), '12309')
        self.assertEqual(convert.format_year(901, 0), '90100')

    def test_format_date_bad_year(self):
        self.assertEqual(convert.format_date(-1, 1, 31), '')
        
    def test_format_date_bad_month(self):
        self.assertEqual(convert.format_date(2015, -1, 1), '')
        self.assertEqual(convert.format_date(2015, 0, 1), '')
        self.assertEqual(convert.format_date(2015, 13, 1), '')
        self.assertEqual(convert.format_date(2015, 14, 1), '')

    def test_format_date_bad_day_of_month(self):
        # Impossible value for dayOfMonth.
        self.assertEqual(convert.format_date(2015, 1, -1), '')
        self.assertEqual(convert.format_date(2015, 1, 0), '')
        self.assertEqual(convert.format_date(2015, 1, 32), '')

        # Too large of dayOfMonth for the given month (months with 30 days).
        for month in [4, 6, 9, 11]:
            self.assertEqual(convert.format_date(2015, month, 31), '')

        # Too large of dayOfMonth for a non-leap year in february
        self.assertEqual(convert.format_date(2015, 2, 29), '')

    def test_format_date_four_digit_year(self):
        self.assertEqual(convert.format_date(1111, 1, 31), '1111-01-31')
        self.assertEqual(convert.format_date(2015, 1, 31), '2015-01-31')
        self.assertEqual(convert.format_date(9999, 1, 31), '9999-01-31')

    def test_format_date_three_digit_year(self):
        self.assertEqual(convert.format_date(111, 1, 31), '2111-01-31')
        self.assertEqual(convert.format_date(215, 1, 31), '2215-01-31')
        self.assertEqual(convert.format_date(999, 1, 31), '2999-01-31')

    def test_format_date_two_digit_year(self):
        target = datetime.datetime(2014, 1, 1)
        with mock_datetime.mock_datetime_today(target, datetime):
            self.assertEqual(datetime.date.today().year, 2014)
        #self.assertEqual(convert.format_date(
        #self.assertEqual(convert.format_date(
        #self.assertEqual(convert.format_date(
        #self.assertEqual(convert.format_date(
        #self.assertEqual(convert.format_date(

    def test_format_date_good_month(self):
        self.assertEqual(convert.format_date(2015, 1, 30), '2015-01-30')
        self.assertEqual(convert.format_date(2015, 9, 30), '2015-09-30')
        self.assertEqual(convert.format_date(2015, 12, 30), '2015-12-30')

    def test_format_date_good_day_of_month(self):
        self.assertEqual(convert.format_date(2015, 10, 1), '2015-10-01')
        self.assertEqual(convert.format_date(2015, 10, 9), '2015-10-09')
        self.assertEqual(convert.format_date(2015, 10, 31), '2015-10-31')

    # convert_date() tests.
    def test_convert_date_cannot_parse(self):
        self.assertListEqual(convert.convert_date(''), [])
        self.assertListEqual(convert.convert_date('123456789'), [])
        self.assertListEqual(convert.convert_date('123'), [])
        self.assertListEqual(convert.convert_date('feb th 2015'), [])
        self.assertListEqual(convert.convert_date('abc'), [])
        self.assertListEqual(convert.convert_date('dec'), [])

    def test_convert_date_iso_8601_unambiguous(self):
        self.assertListEqual(convert.convert_date('2015-12-31'),
                             ['2015-12-31'])

        self.assertListEqual(convert.convert_date('2015-01-01'),
                             ['2015-01-01'])

    def test_convert_date_iso_8601_ambiguous(self):
        pass
        # self.assertListEqual(convert.convert_date(),
        #                     [])

if __name__ == '__main__':
    unittest.main()
