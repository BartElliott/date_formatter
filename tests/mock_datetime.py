import datetime
import unittest

real_date_class = datetime.date

def mock_datetime_today(target, dt):
    class DatetimeSubclassMeta(type):
        @classmethod
        def __instancecheck__(mcs, obj):
            return isinstance(obj, real_date_class)

    class BaseMockedDatetime(real_date_class):
        @classmethod
        def today(cls):
            return target

    # Python2 & Python3 compatible metaclass
    MockedDatetime = DatetimeSubclassMeta('datetime', (BaseMockedDatetime,), {})

    return unittest.mock.patch.object(datetime, 'datetime', MockedDatetime)
