'''Converts dates in any format into ISO 8601 format of YYYY-MM-DD.'''


import globals


import datetime
import itertools
import sys


# ----------- Utility functions. --------------
def alpha_count(s):
    return sum([1 for c in s if c.isalpha()])


def is_leap_year(year):
    if year % 4 == 0:
        if year % 400 == 0:
            return True
        if year % 100 == 0:
            return False
        return True
    return False


def days_in_month(month, year):
    if month == 2:
        if is_leap_year(year):
            return 29
        return 28

    if (month == 1 or month == 3 or month == 5 or
            month == 7 or month == 8 or month == 10 or
            month == 12):
        return 31
    if (month == 4 or month == 6 or
            month == 9 or month == 11):
        return 30

    # Should never be reached (for a good 'month' input).
    return 0


def format_year(firstPart, lastTwo):
    if firstPart < 10 and lastTwo < 10:
        return '0{0}0{1}'.format(firstPart, lastTwo)
    elif firstPart < 10:
        return '0{0}{1}'.format(firstPart, lastTwo)
    elif lastTwo < 10:
        return '{0}0{1}'.format(firstPart, lastTwo)
    else:
        return '{0}{1}'.format(firstPart, lastTwo)


def format_date(year, month, dayOfMonth):
    '''Takes the 3 components of a date and returns a single ISO 8601
    formatted date.'''

    if (month < 1 or month > 12 or
            dayOfMonth < 1 or dayOfMonth > 31 or 
            year < 0):
        return ''

    strYear = str(year)
    strMonth = str(month)
    strDayOfMonth = str(dayOfMonth)

    currYearFull = datetime.date.today().year
    currYearFirstTwo = currYearFull // 100
    currYearLastTwo = currYearFull % 100

    if year < 100:
        # If the 2 digits are close in the future assume it's the current
        # century, but assume the previous century if it's very large
        # (99 -> 1999, 17 -> 2017).
        if currYearLastTwo < 90:
            if currYearLastTwo <= year <= currYearLastTwo + 10:
                strYear = format_year(currYearFirstTwo, year)
            elif currYearLastTwo - year > 0:
                strYear = format_year(currYearFirstTwo, year)
            else:
                strYear = format_year(currYearFirstTwo - 1, year)
        else:
            if year >= currYearLastTwo:
                # The given 'year' is greater than 90 as well, so it's in the
                # same century.
                strYear = format_year(currYearFirstTwo, year)
            elif 0 <= year <= currYearLastTwo % 10:
                strYear = format_year(currYearFirstTwo + 1, year)
            else:
                strYear = format_year(currYearFirstTwo, year)
    elif year < 1000:
        # Assume 3 digit years have the same first digit as the current year.
        strYear = '{0}{1}'.format(currYearFirstTwo // 10, year)

    if month < 10:
        strMonth = '0{0}'.format(month)

    if dayOfMonth < 10:
        strDayOfMonth = '0{0}'.format(dayOfMonth)

    predictedDaysInMonth = days_in_month(int(strMonth), int(strYear))
    if int(strDayOfMonth) > predictedDaysInMonth:
        return ''

    return '{0}-{1}-{2}'.format(strYear, strMonth, strDayOfMonth)


# ----------- These functions all try to parse a date differently. ------------
def try_parse_alphabetic(s):
    pass


def try_parse_numeric(s):
    '''Try to split the string with the various delimiters that are used
    to separate the day, month and year.'''
    elements = s.split('-')
    if len(elements) != 3:
        elements = s.split('/')
    if len(elements) != 3:
        elements = s.split('.')
    if len(elements) != 3:
        # Out of ideas to parse the non-alphabetic date, so give up.
        return []

    elements = [int(x) for x in elements]

    # Now 'elements' has what should be a dayOfMonth, month, and year
    # in some order.
    dayOfMonth = 0
    month = 0
    year = 0

    if max(elements) <= globals.LONGEST_MONTH:
        if 0 in elements:
            # Treat the 0 as the year.
            elements.remove(0)
            if 0 in elements:
                # Neither month nor dayOfMonth can be 0. Error.
                return []
            result = []
            for (m, d) in set(itertools.permutations(elements, 2)):
                newDate = format_date(0, m, d)
                if newDate != '':
                    result.append(newDate)
                return result
        # All 3 elements could be any of the 3 parts of the date. All
        # permutations of the elements could be the date.
        result = []
        for (y, m, d) in set(itertools.permutations(elements, 3)):
            newDate = format_date(y, m, d)
            if newDate != '':
                result.append(newDate)
        return result

    for item in elements:
        # Try to find one big enough to guarantee that it's the year.
        if item > globals.LONGEST_DAY_OF_THE_MONTH:
            year = item
            elements.remove(item)
            break

    if year > 0:
        for item in elements:
            if item > globals.LONGEST_MONTH:
                dayOfMonth = item
                elements.remove(item)
                break
        if dayOfMonth > 0:
            # Found a unique date. Return it.
            month = elements[0]

            newDate = format_date(year, month, dayOfMonth)
            if newDate != '':
                return [newDate]
        else:
            # There was a unique year but not a unique dayOfMonth.
            result = []
            for (m, d) in set(itertools.permutations(elements, 2)):
                newDate = format_date(year, m, d)
                if newDate != '':
                    result.append(newDate)
            return result
    else:
        # No number was large enough to guarantee that it's the year, but
        # there's at least one that is the dayOfMonth (since the max is at
        # least LONGEST_MONTH + 1).

        if 0 in elements:
            year = 0
            elements.remove(0)
            if 0 in elements:
                # Neither month nor dayOfMonth can be 0. Error.
                return []
            if (1 <= min(elements) <= 12 and 13 <= max(elements) <= 31):
                month = min(elements)
                dayOfMonth = max(elements)

                newDate = format_date(year, month, dayOfMonth)
                if newDate != '':
                    return [newDate]
                return []
            else:
                # An assumption in the code above was false. This is an issue.
                sys.exit('try_parse_numeric(): problem parsing input (input = '
                         '{0}.'.format(s))

        if min(elements) > globals.LONGEST_MONTH:
            return []

        # Now figure out how many could be the month.
        if sum([1 for x in elements if 1 <= x <= globals.LONGEST_MONTH]) == 1:
            # Only one can be the month. The other 2 (13 <= x <= 31) can be
            # either the year or the dayOfMonth.
            month = min(elements)
            elements.remove(month)

            result = []
            for (y, d) in set(itertools.permutations(elements, 2)):
                newDate = format_date(y, month, d)
                if newDate != '':
                    result.append(newDate)
            return result
        else:
            # 2 can be the month, dayOfMonth, or year (1 <= x <= 12), and the
            # other one (13 <= x <= 31) can only be the year or dayOfMonth.
            # The following does the 4 possible permutations by hand.
            biggest = max(elements)
            elements.remove(biggest)
            result = []

            curr = format_date(biggest, elements[0], elements[1])
            if curr != '':
                result.append(curr)
            curr = format_date(biggest, elements[1], elements[0])
            if curr != '':
                result.append(curr)
            curr = format_date(elements[0], elements[1], biggest)
            if curr != '':
                result.append(curr)
            curr = format_date(elements[1], elements[0], biggest)
            if curr != '':
                result.append(curr)
            return result


def convert_date(s):
    '''Takes a string s that could be any in any format and tries to interpret
    it as a date.
    Returns one of:
        an empty list if the input cannot be interpreted as a date
        a list of one string in YYYY-MM-DD format if there's no ambiguity
        a list of multiple strings in YYYY-MM-DD format if any of them could be
            what the user intended.'''

    if alpha_count(s) > 0:
        # Try to parse the string as words that represent a date.
        return try_parse_alphabetic(s)
    else:
        # Try to parse the string as numbers that represent a date.
        return try_parse_numeric(s)


# -------------------- Main -----------------------
def main():
    if len(sys.argv) == 1:
        # No arguments given to script. Ask for input.
        inputPrompt = 'Input a date to convert, or press enter to quit: '

        s = input(inputPrompt)
        while s != '':
            print('Result: {0}.'.format(convert_date(s)))
            s = input(inputPrompt)
    else:
        print('Args: {0}'.format(sys.argv[1:]))
        result = [convert_date(s) for s in sys.argv[1:]]
        print('Result: {0}'.format(result))
        return result


if __name__ == '__main__':
    main()
